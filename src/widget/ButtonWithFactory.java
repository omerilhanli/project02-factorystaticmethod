package widget;

/*
            * Static Factory Method *

            - Üretim aşamasını kontrol altına alırız. Exception handling de dahil bir çok kontrol mekanizması ekleyebiliriz.
            - Instance üretirken bir yada daha çok veya birbirinden farklı parametreler için daha anlamlı isimler kullanabiliriz.
            - Constructor ile direkt teması engellediğimiz için her defasında instance verme zorunluluğunu kaldırırız.
            // Mesela singleton patterni ile alacağımız instance'ı her seferinde null check kontrol ile
              'new statement' ına erişimi engelleriz. Dolayısıyla sistemde üretilmiş objemiz kullanılmaya devam eder.

            - Normalde 'new' ile aldığımız instance'lar için aynı imzaya sahip farklı constructorları
              parametre tiplerini veya yerlerini değiştirerek daha karışık görünüme sahip yapılar kurmak zorunda kalırız.

              - - - Static Factory Method bize bu sorunları çözmede yardımcı olur √
 */
public class ButtonWithFactory {

    private String txtButton;

    private String colorButton;

    private int sizeButton;

    public ButtonWithFactory(String txtButton, String colorButton, int sizeButton) {

        this.txtButton = txtButton;

        this.colorButton = colorButton;

        this.sizeButton = sizeButton;
    }

    public static ButtonWithFactory createFull(String txtButton, String colorButton, int sizeButton) {

        return new ButtonWithFactory(txtButton, colorButton, sizeButton);
    }

    public static ButtonWithFactory createNameAndSize(String txtButton, int sizeButton) {

        return new ButtonWithFactory(txtButton, null, sizeButton);
    }

    public static ButtonWithFactory createNameAndColor(String txtButton, String colorButton) {

        return new ButtonWithFactory(txtButton, colorButton, 0);
    }

    public static ButtonWithFactory createWithJustName(String txtButton) {

        return new ButtonWithFactory(txtButton, null, 0);
    }

    public static ButtonWithFactory createDefault() {

        return new ButtonWithFactory(null, null, 0);
    }


    @Override
    public String toString() {
        return "ButtonWithFactory{" +
                "txtButton='" + txtButton + '\'' +
                ", colorButton='" + colorButton + '\'' +
                ", sizeButton=" + sizeButton +
                '}';
    }
}
