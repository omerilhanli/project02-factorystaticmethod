package widget;

public class Button {

    private String txtButton;

    private String colorButton;

    private int sizeButton;

    public Button() {

    }

    public Button(String txtButton) {

        this.txtButton = txtButton;
    }

    public Button(String txtButton, int sizeButton) {

        this.txtButton = txtButton;

        this.sizeButton = sizeButton;
    }

    public Button(String txtButton, String colorButton) {

        this.txtButton = txtButton;

        this.colorButton = colorButton;
    }

    public Button(String txtButton, String colorButton, int sizeButton) {

        this.txtButton = txtButton;

        this.colorButton = colorButton;

        this.sizeButton = sizeButton;
    }

    @Override
    public String toString() {
        return "Button{" +
                "txtButton='" + txtButton + '\'' +
                ", colorButton='" + colorButton + '\'' +
                ", sizeButton=" + sizeButton +
                '}';
    }
}
