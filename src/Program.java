import widget.Button;
import widget.ButtonWithFactory;

public class Program {


    public static void main(String[] args) {

        createButtonsStandart();
        //createButtonsStaticFactory();

    }

    static void createButtonsStandart() {

        Button button1 = new Button();

        Button button2 = new Button("Kaydet2");

        Button button3 = new Button("Kaydet3", "Mavi3");

        Button button4 = new Button("Kaydet4", 16);

        Button button5 = new Button("Kaydet5", "Kırmızı5", 24);

        writeConsole(button1, button2, button3, button4, button5);
    }

    static void createButtonsStaticFactory() {

        ButtonWithFactory buttonWithFactory1 = ButtonWithFactory.createDefault();

        ButtonWithFactory buttonWithFactory2 = ButtonWithFactory.createWithJustName("Kaydet2");

        ButtonWithFactory buttonWithFactory3 = ButtonWithFactory.createNameAndColor("Kaydet3", "Mavi3");

        ButtonWithFactory buttonWithFactory4 = ButtonWithFactory.createNameAndSize("Kaydet4", 16);

        ButtonWithFactory buttonWithFactory5 = ButtonWithFactory.createFull("Kaydet5", "Kırmızı5", 24);

        writeConsole(buttonWithFactory1, buttonWithFactory2, buttonWithFactory3, buttonWithFactory4, buttonWithFactory5);
    }


    static void writeConsole(Button... buttons) {
        for (Button button : buttons) {
            System.out.println(button.toString());
        }
    }

    static void writeConsole(ButtonWithFactory... buttons) {
        for (ButtonWithFactory button : buttons) {
            System.err.println(button.toString());
        }
    }


}
